package com.hackathon.hackathon.controller;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    public void orderControllerFindAllSuccess() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Order(2)
    public void orderControllerFindAllNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/orderz"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Order(3)
    public void orderControllerCreateOrder() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/v1/order/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\r\n  \"buyOrSell\": \"sell\",\r\n  \"id\": 2,\r\n  \"price\": 650,\r\n  \"statusCode\": 0,\r\n  \"stockTicker\": \"AMZN\",\r\n  \"volume\": 34\r\n}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    @Order(4)
    public void orderControllerUpdateOrder() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/v1/order/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\r\n  \"buyOrSell\": \"sell\",\r\n  \"id\": 1,\r\n  \"price\": 500,\r\n  \"statusCode\": 0,\r\n  \"stockTicker\": \"AMZN\",\r\n  \"volume\": 34\r\n}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Order(5)
    public void orderControllerUpdateOrderNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(post("/api/v1/order/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\r\n  \"buyOrSell\": \"sell\",\r\n  \"id\": 5,\r\n  \"price\": 500,\r\n  \"statusCode\": 0,\r\n  \"stockTicker\": \"AMZN\",\r\n  \"volume\": 34\r\n}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Order(6)
    public void orderControllerFindByID() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Order(7)
    public void orderControllerFindByIDNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/50"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Order(8)
    public void orderControllerFindByPrice() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/getByPrice/500"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Order(9)
    public void orderControllerFindByPriceNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/getByPrice/99999"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Order(10)
    public void orderControllerFindByStockTickerSuccess() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/getByStockTicker/AMZN"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Order(11)
    public void orderControllerFindByStockTickerNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/order/getByStockTicker/pizza"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @Order(12)
    public void orderControllerDeleteOrder() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(delete("/api/v1/order/removebyid/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }


}
