package com.hackathon.hackathon.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {

    private static final int testPrice = 100;
    private static final String testStockTicker = "AMZN";
    private static final int testvolume = 3;
    private Order order;

    @BeforeEach
    public void setup() {
        this.order = new Order();
    }

    @Test
    public void setPriceTest() {
        this.order.setPrice(100);

        assertEquals(testPrice, this.order.getPrice());
    }

    @Test
    public void setStockTickerTest() {
        this.order.setStockTicker("AMZN");
        assertEquals(testStockTicker, this.order.getStockTicker());
    }


    @Test
    public void setVolumeTest() {
        this.order.setVolume(3);

        assertEquals(testvolume, this.order.getVolume());
    }
}
