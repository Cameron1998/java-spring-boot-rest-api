package com.hackathon.hackathon.service;

import com.hackathon.hackathon.repository.OrderRepository;
import com.hackathon.hackathon.entities.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    public List<Order> findAll() {

        return orderRepository.findAll();
    }

    public Order findById(long id){

        return orderRepository.findById(id).get();

    }

    public Order update(Order shipper) {
        orderRepository.findById(shipper.getId()).get();
        shipper.setEditedTimestamp(new Date(System.currentTimeMillis()));
        return orderRepository.save(shipper);
    }

    public Order save(Order order) {
        return orderRepository.save(order);
    }

    public void delete(Order order) {
        orderRepository.delete(order);
    }

    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }

    public List<Order> findByStockTicker(String stockTicker){
        return orderRepository.findByStockTicker(stockTicker);
    }


    public List<Order> findByPrice(double price) {
        return orderRepository.findByPrice(price);
    }

    public List<Order> compareValueToOrderPrice(double price){
        List<Order> listOfOrdersAbovePrice = new ArrayList<Order>();
        for (int i = 0; i < findAll().size(); i++) {

            if(findAll().get(i).getPrice() >= price){
                listOfOrdersAbovePrice.add(findAll().get(i));
            }
        }
        return listOfOrdersAbovePrice ;
    }

    public List<Order> findOrderByStatusCode(int statusCode){
        return orderRepository.findByStatusCode(statusCode);
    }


    public int findTotalStock(String stockTicker){
        int totalStock = 0;
        List<Order> listOfStocks = new ArrayList<>();
        for(int i = 0; i < findAll().size(); i++){
            if(findAll().get(i).getStockTicker().equalsIgnoreCase(stockTicker)){
                listOfStocks.add(findAll().get(i));
            }
        }
        for(Order o : listOfStocks){
            totalStock += o.getVolume();
        }

        return totalStock;
    }

    public HashMap<String, Integer> summaryOfStockOwned(){
        List<String> stocks = new ArrayList<String>();
        List<Integer> volume = new ArrayList<Integer>();
        String stockTicker = "";
        int stockTickerIndex = 0;
        int stockVolume = 0;
        int currentVolume = 0;
        for (int i = 0; i < findAll().size(); i++) {
            stockTicker = findAll().get(i).getStockTicker();
            stockVolume = findAll().get(i).getVolume();
            if(stocks.contains(findAll().get(i).getStockTicker()) ) {
                stockTickerIndex = stocks.indexOf(stockTicker);
                currentVolume = volume.get(stockTickerIndex);
                if(findAll().get(i).getBuyOrSell().equals("buy")){
                    volume.set(stockTickerIndex,currentVolume + stockVolume);
                }
                else{
                    volume.set(stockTickerIndex,currentVolume - stockVolume);
                }
            }
            else {
                stocks.add(stockTicker);
                if(findAll().get(i).getBuyOrSell().equals("buy")) {
                    volume.add(stockVolume);
                }
                else{
                    volume.add(-stockVolume);
                }
            }
        }
        HashMap<String, Integer> hmap = new HashMap<String, Integer>();
        for(int i = 0; i <stocks.size();i++){
            hmap.put(stocks.get(i), volume.get(i));
        }
        return hmap;
    }
}
