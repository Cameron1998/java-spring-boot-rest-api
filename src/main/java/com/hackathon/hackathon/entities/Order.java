package com.hackathon.hackathon.entities;

import java.util.Date;
import javax.persistence.*;

@Entity(name="Orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="stockTicker")
    String stockTicker;

    @Column(name="price")
    double price;

    @Column(name="volume")
    int volume;

    @Column(name="buyOrSell")
    String buyOrSell;

    @Column(name="statusCode")
    int statusCode;

    @Column(name="date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTimestamp = new Date(System.currentTimeMillis());

    @Column(name="dateEdited")
    @Temporal(TemporalType.TIMESTAMP)
    private Date editedTimestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Date getEditedTimestamp() {
        return editedTimestamp;
    }

    public void setEditedTimestamp(Date editedTimestamp) {
        this.editedTimestamp = editedTimestamp;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                ", createdTimestamp=" + createdTimestamp +
                '}';
    }
}
