package com.hackathon.hackathon.repository;
import com.hackathon.hackathon.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByStockTicker(String stockTicker);
    List<Order> findByPrice(double price);
    List<Order> findByStatusCode(int statusCode);

}
