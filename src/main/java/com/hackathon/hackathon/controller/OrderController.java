package com.hackathon.hackathon.controller;

import com.hackathon.hackathon.entities.Order;
import com.hackathon.hackathon.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/order")
public class OrderController {

    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    // Find all orders method
    @GetMapping
    public List<Order> findAll(){
        return orderService.findAll();
    }

    @GetMapping("/stockSummary")
    public HashMap<String, Integer> summaryOfStockOwned(){
        return orderService.summaryOfStockOwned();
    }

    // Find order by ID method
    @GetMapping("/{id}")
    public ResponseEntity<Order> findById(@PathVariable Long id){
        try {
            return new ResponseEntity<Order>(orderService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex){
            // return 404
            LOG.debug("No orders with id :" +"["+id+"]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }


    @GetMapping("/getByPriceRange/{price}")
    public ResponseEntity<List<Order>> findOrderByPriceOverValue(@PathVariable double price){
        try {
            return new ResponseEntity<List<Order>>(orderService.compareValueToOrderPrice(price), HttpStatus.OK);
        } catch(NoSuchElementException ex){
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }

    @GetMapping("/getByPrice/{price}")
    public ResponseEntity<List<Order>> findByPrice(@PathVariable double price){
        try {
            if(orderService.findByPrice(price).isEmpty()){
                throw new NoSuchElementException();
            }else{
                return new ResponseEntity<List<Order>>(orderService.findByPrice(price), HttpStatus.OK);
            }
        } catch(NoSuchElementException ex){
            // return 404
            LOG.debug("No orders with price above :" +"["+price+"]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }


    @GetMapping("/getByStockTicker/{stockTicker}")
    public ResponseEntity<List<Order>> findByStockTicker(@PathVariable String stockTicker){
        try {
            if(orderService.findByStockTicker(stockTicker).isEmpty()){
                throw new NoSuchElementException();
            }else{
                return new ResponseEntity<List<Order>>(orderService.findByStockTicker(stockTicker),HttpStatus.OK);
            }
        } catch(NoSuchElementException ex){
            // return 404
            LOG.debug("Unknown Stock ticker :" +"["+stockTicker+"]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }


    @GetMapping("/getTotalStockByStockTicker/{stockTicker}")
    public int findStockByStockTicker (@PathVariable String stockTicker){
            return orderService.findTotalStock(stockTicker);
    }



    @GetMapping("/getByStatusCode/{statusCode}")
    public ResponseEntity<List<Order>> findByStatusCode(@PathVariable int statusCode){
        try {
            if(orderService.findOrderByStatusCode(statusCode).isEmpty()){
                throw new NoSuchElementException();
            }else{
                return new ResponseEntity<List<Order>>(orderService.findOrderByStatusCode(statusCode),HttpStatus.OK);
            }        } catch(NoSuchElementException ex){
            // return 404
            LOG.debug("Unknown Status code :" +"["+statusCode+"]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }


    // Create order method
    @PostMapping("/save")
    public ResponseEntity<Order> create(@RequestBody Order order){

        return new ResponseEntity<Order>(orderService.save(order), HttpStatus.CREATED);


    }

    // Update order method
    @PostMapping("/update")
    public ResponseEntity<Order> update(@RequestBody Order order) {
        try {
            return new ResponseEntity<Order>(orderService.update(order), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + order + "]");
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    // Delete order method

    @DeleteMapping("/removebyid/{id}")
    public void deleteById(@PathVariable(value = "id") Long id){
        orderService.deleteById(id);
    }



}
