CREATE Database IF NOT EXISTS hackathon;
USE hackathon;

CREATE TABLE IF NOT EXISTS orders(
     id               INTEGER  NOT NULL PRIMARY KEY
    ,stockTicker      VARCHAR(4) NOT NULL
    ,price            INTEGER  NOT NULL
    ,volume           INTEGER  NOT NULL
    ,buyOrSell        VARCHAR(4) NOT NULL
    ,statusCode       INTEGER  NOT NULL
    ,date VARCHAR(29) NOT NULL,
    ,dateEdited       VARCHAR (29)
    );

INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (1,'GOOG',2828,200,'buy',0,'2021-10-15T12:34:40.000+00:00');
INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (2,'AMZN',3428,100,'buy',1,'2021-10-15T12:40:35.000+00:00');
INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (3,'FB',328,3600,'buy',0,'2021-10-15T12:40:58.000+00:00');
INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (4,'MSFT',100,33,'sell',3,'2021-10-15T12:41:04.000+00:00');
INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (5,'TSLA',800,10000,'buy',2,'2021-10-15T12:42:17.000+00:00');
INSERT INTO orders(id,stockTicker,price,volume,buyOrSell,statusCode,date) VALUES (6,'V',225,555,'sell',1,'2021-10-15T12:42:24.000+00:00');


